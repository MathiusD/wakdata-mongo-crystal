require "../spec_helper"

describe Wakdata::CryoMongo do
  it "state", tags: ["need-local_server"] do
    state_data = [
      Wakdata::State.new(
        Wakdata::Definition::State.new(1),
        nil,
        nil
      ),
      Wakdata::State.new(
        Wakdata::Definition::State.new(2),
        Wakdata::Traduction.new(
          "Example",
          "Example",
          "TODO",
          "TODO"
        ),
        nil
      ),
    ]
    Wakdata::State.put_in_mongo MONGO_DB, "state", state_data
    Wakdata::State.get_from_mongo(MONGO_DB, "state").should eq(state_data)
  end
end
