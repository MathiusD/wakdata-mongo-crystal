require "spec"
require "json"
require "../src/wakdata/cryomongo"

struct SpecMongoConfig
  include JSON::Serializable
  include JSON::Serializable::Strict

  @[JSON::Field(key: "mongo_connection_string")]
  getter mongo_connection_string : String

  def initialize(@mongo_connection_string : String)
  end
end

SPEC_MONGO_CONFIG_FILE = File.open("./spec/resources/spec-mongo-config.json")

SPEC_MONGO_CONFIG = SpecMongoConfig.from_json(SPEC_MONGO_CONFIG_FILE)

MONGO_CLIENT = Mongo::Client.new SPEC_MONGO_CONFIG.mongo_connection_string
MONGO_DB     = MONGO_CLIENT["wakdata"]
