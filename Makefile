default_target: fetch_data

clean:
	@rm -rf docs
	@rm -rf lib
	@rm -rf reports
	@rm -f shard.lock

install:
	@shards install

format:
	@crystal tool format --check

tests: regular_tests

regular_tests:
	@crystal spec --tag ~integration --junit_output "reports/regular_tests-$(shell date '+%s').xml"

docs:
	@crystal docs lib/wakdata/src/wakdata.cr src/wakdata/cryomongo.cr

launch_mongo_local_server:
	@docker run --rm -p 27017:27017 mongo