# Wakdata::CryoMongo - Crystal

[![Pipeline Status](https://gitlab.com/MathiusD/wakdata-cryomongo-crystal/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/wakdata-cryomongo-crystal/-/pipelines)
[![Documentation](https://img.shields.io/badge/docs-available-brightgreen.svg)](https://mathiusd.gitlab.io/wakdata-cryomongo-crystal)

Wakdata::CryoMongo is crystal module for interact with public data of MMORPG Wakfu from Mongo Database
