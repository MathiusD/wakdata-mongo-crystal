require "cryomongo"
require "json"
require "wakdata"
require "git-version-gen/src/git-version-gen.cr"

# Wakdata is module for fetch and use public data gived by Wakfu Team
module Wakdata::CryoMongo
  GitVersionGen.generate_version __DIR__

  macro attach_mongo_methods(struct_name)
    Wakdata::CryoMongo.create_bson_record {{struct_name}}
    Wakdata::CryoMongo.attach_get_from_mongo Wakdata::CryoMongo::BSON_{{struct_name}}, {{struct_name}}
    Wakdata::CryoMongo.attach_put_in_mongo Wakdata::CryoMongo::BSON_{{struct_name}}, {{struct_name}}
  end

  macro create_bson_record(struct_name)
    record Wakdata::CryoMongo::BSON_{{struct_name}},
      data : {{struct_name}},
      _id : BSON::ObjectId = BSON::ObjectId.new,
      creation_date : Time = Time.utc do
      include BSON::Serializable
      include JSON::Serializable
    end
  end

  macro attach_get_from_mongo(bson_record_name, struct_name)
    def {{struct_name}}.get_from_mongo(
      mongo_db : Mongo::Database,
      collection_name : String
    ) : Array({{struct_name}})
      collection = mongo_db[collection_name]
      cursor = collection.find
      datas_retrieved = Array({{struct_name}}).new
      cursor.of({{bson_record_name}}).to_a.each do |bson_data|
        datas_retrieved << bson_data.data
      end
      return datas_retrieved
    end
  end

  macro attach_put_in_mongo(bson_record_name, struct_name)
    def {{struct_name}}.put_in_mongo(
      mongo_db : Mongo::Database,
      collection_name : String,
      data : Array({{struct_name}})
    )
      collection = mongo_db[collection_name]
      data_inserted = Array({{bson_record_name}}).new
      data.each do |single_data|
        data_inserted << {{bson_record_name}}.new data: single_data
      end
      collection.insert_many(data_inserted)
    end
  end
end

Wakdata::CryoMongo.attach_mongo_methods Wakdata::Action
Wakdata::CryoMongo.attach_mongo_methods Wakdata::Blueprint
Wakdata::CryoMongo.attach_mongo_methods Wakdata::CollectibleResource
Wakdata::CryoMongo.attach_mongo_methods Wakdata::HarvestLoot
Wakdata::CryoMongo.attach_mongo_methods Wakdata::ItemProperty
Wakdata::CryoMongo.attach_mongo_methods Wakdata::Item
Wakdata::CryoMongo.attach_mongo_methods Wakdata::ItemType
Wakdata::CryoMongo.attach_mongo_methods Wakdata::JobItem
Wakdata::CryoMongo.attach_mongo_methods Wakdata::RecipeCategory
Wakdata::CryoMongo.attach_mongo_methods Wakdata::RecipeIngredient
Wakdata::CryoMongo.attach_mongo_methods Wakdata::RecipeResult
Wakdata::CryoMongo.attach_mongo_methods Wakdata::Recipe
Wakdata::CryoMongo.attach_mongo_methods Wakdata::Resource
Wakdata::CryoMongo.attach_mongo_methods Wakdata::ResourceType
Wakdata::CryoMongo.attach_mongo_methods Wakdata::State
